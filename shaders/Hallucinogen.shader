shader_type canvas_item;

uniform float power = 0.0;

void fragment() {
	float scale = 0.01;
	vec2 offset = vec2(sin(TIME + SCREEN_UV.y*pow(power,1.0/13.0))*(power/1000.0));
	vec3 c = textureLod(SCREEN_TEXTURE, SCREEN_UV + offset, 0.0).rgb;
	vec3 c2 = textureLod(SCREEN_TEXTURE, SCREEN_UV + offset*1.1, 0.0).rgb;
	vec3 c3 = textureLod(SCREEN_TEXTURE, SCREEN_UV + offset*0.9, 0.0).rgb;
	COLOR.r = c.r;
	COLOR.g = c2.g;
	COLOR.b = c3.b;
}