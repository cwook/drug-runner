shader_type canvas_item;
uniform float power = 0.0;

void vertex() {
	VERTEX.y = VERTEX.y + 4.0*(1.0+sin(5.0*TIME + UV.x*22.0));
}
/*
void fragment() {
	vec2 offset = vec2(sin(TIME) * 0.01 * power, cos(TIME) * 0.01 * power);
	vec3 c = textureLod(TEXTURE, UV + offset/2.0, 0.0).rgb;
	vec3 c2 = textureLod(TEXTURE, UV + offset, 0.0).rgb;
	vec3 c3 = textureLod(TEXTURE, UV + offset/4.0, 0.0).rgb;
	//COLOR.r = c.r;
	//COLOR.g = c2.g;
	//COLOR.b = c3.b;
}