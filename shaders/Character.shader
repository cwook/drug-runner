shader_type canvas_item;
render_mode blend_mix;

uniform vec3 skin_col = vec3(1,1,0);
uniform vec3 sleeve_col = vec3(1,1,0);
uniform vec3 torso_col = vec3(1,1,0);
uniform vec3 shorts_col = vec3(1,1,0);
uniform vec3 eye_col = vec3(0.2,0.2,0.8);

bool colors_equal(in vec3 a, in vec3 b)
{
	return (length(a-b) < 0.0001);
}

void fragment()
{
	vec4 col = texture(TEXTURE, UV.xy);
	COLOR = col;
	if (colors_equal(col.rgb, vec3(0.84313725490196078431372549019608,0.48235294117647058823529411764706,0.72941176470588235294117647058824)))
	{
		COLOR = vec4(skin_col,col.a);
	} else if (colors_equal(col.rgb, vec3(0.85098039215686274509803921568627,0.34117647058823529411764705882353,0.38823529411764705882352941176471)))
	{
		COLOR = vec4(skin_col*0.8,col.a);
	} else if (colors_equal(col.rgb, vec3(0.41568627450980392156862745098039,0.74509803921568627450980392156863,0.18823529411764705882352941176471)))
	{
		COLOR = vec4(sleeve_col,col.a);
	} else if (colors_equal(col.rgb, vec3(0.29411764705882352941176470588235,0.41176470588235294117647058823529,0.18431372549019607843137254901961)))
	{
		COLOR = vec4(sleeve_col*0.8,col.a);
	} else if (colors_equal(col.rgb, vec3(0.18823529411764705882352941176471,0.37647058823529411764705882352941,0.50980392156862745098039215686275)))
	{
		COLOR = vec4(torso_col,col.a);
	} else if (colors_equal(col.rgb, vec3(1.0,1.0,1.0)))
	{
		COLOR = vec4(shorts_col,col.a);
	} else if (colors_equal(col.rgb, vec3(0.0,0.0,1.0)))
	{
		COLOR = vec4(eye_col,col.a);
	}
}