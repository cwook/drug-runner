tool

extends KinematicBody2D

class_name Character

const JUMP_LEN = 35
var jump_counter = 0.0
var jumping = false
var fallen = false
export(int, 0, 4) var lane = 0
var speed = 30
var velocity = Vector2()

func _ready():
	randomize()
	
	$Sprite.material = ShaderMaterial.new()
	$Sprite.material.shader = preload("res://shaders/Character.shader")
	var skinRed = pow(float(120 + randi()%135)/255.0, 1.0)
	$Sprite.material.set_shader_param("skin_col", Vector3(skinRed, skinRed * 0.8, skinRed * 0.6))
	$Sprite.material.set_shader_param("sleeve_col", Vector3(randf(),randf(),randf()))
	$Sprite.material.set_shader_param("torso_col", Vector3(randf(),randf(),randf()))
	$Sprite.material.set_shader_param("shorts_col", Vector3(randf(),randf(),randf()))
	var eyeRand = pow(float(20 + (randi()%220)) / 255.0, 5.0)
	var eyeCol = Color.from_hsv(eyeRand, eyeRand * 0.9, eyeRand * 0.7)
	$Sprite.material.set_shader_param("eye_col", Vector3(eyeCol.r,eyeCol.g,eyeCol.b))
	
	position.y = -45 + lane * 25

func _process(dt):
	if !Engine.editor_hint:
		velocity = Vector2()
		if !fallen:
			if jumping:
				jump_counter += dt * speed
			if jump_counter >= JUMP_LEN:
				jump_counter = 0.0
				$AnimationPlayer.play("Run")
				jumping = false
				$CollisionShape2D.disabled = false
		
		$AnimationPlayer.playback_speed = speed / 20.0
		
		_update(dt)
		
		velocity.x = (Track.speed - speed) * dt
		lane = clamp(lane,0,4)
		var desired_y = -45 + lane * 25
		velocity.y = (desired_y - position.y) * dt * 12
		move_and_collide(velocity)
	else:
		position.y = -45 + lane * 25
	

func _update(dt):
	pass

func jump():
	$AnimationPlayer.play("Jump")
	$CollisionShape2D.disabled = true
	jump_counter = 0.0
	jumping = true

func fall():
	speed = 0
	self.position.x -= 15
	$AnimationPlayer.play("Fall")
	$CollisionShape2D.disabled = true
	Track.add_to_dead_characters(self)
	fallen = true


