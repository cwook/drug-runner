extends Node2D

enum {BOOT, MENU, PLAYING, RESULT}
var game_in_progress = false
var state = BOOT
onready var HUD = get_node("HUD")

func _ready():
	set_process(true)

func _process(dt):
	match state:
		BOOT:
			get_tree().paused = true
		MENU:
			get_tree().paused = true
			if Input.is_action_just_pressed("pause") and game_in_progress:
				state = PLAYING
		PLAYING:
			get_tree().paused = false
			if Input.is_action_just_pressed("pause"):
				state = MENU
		RESULT:
			get_tree().paused = true
			if Input.is_action_just_pressed("pause") and game_in_progress:
				state = MENU

func new_game():
	game_in_progress = true
	Track.new_game()
	state = PLAYING