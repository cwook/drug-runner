extends Node2D

var packed_hurdle = preload("res://scenes/hurdle.tscn")
var packed_heroin = preload("res://scenes/powerups/Heroin.tscn")
var packed_bennies = preload("res://scenes/powerups/Bennies.tscn")
var packed_acid = preload("res://scenes/powerups/Acid.tscn")
var packed_character = preload("res://scenes/Character.tscn")
onready var player = get_node("Characters/Player")


var speed = 30.0
var dist = 0.0
var total_dist = 0.0
#var Game
var HUD

func _ready():
	#Game = get_node("../Game")
	HUD = get_node("../Game/HUD")
	gen_256(32)
	set_process(true)

func gen_256(x_offset):
	randomize()
	for i in range(0,5): # which lane
		for j in range(0,10): # which x coord
			var r = randi()
			if r % 3 == 0:
				var hurd = packed_hurdle.instance()
				hurd.position.y = -50.0 + i * 25.0
				hurd.position.x = -160.0 - 64.0 * j + x_offset
				$Hurdles.add_child(hurd)
			elif r % 5 == 0:
				var powerup = null
				var typeFlip = randi()%3
				if typeFlip == 0:
					powerup = packed_heroin.instance()
				elif typeFlip == 1:
					powerup = packed_bennies.instance()
				else:
					powerup = packed_acid.instance()
				powerup.position.y = -50.0 + i * 25.0
				powerup.position.x = -128.0 - 64.0 * j + x_offset
				$Powerups.add_child(powerup)

func _process(dt):
	dist -= dt * speed
	if !player.fallen:
		total_dist += dt * speed
		HUD.total_dist = int(total_dist / 10.0)
	$BG/TrackSprite.region_rect.position.x = dist
	$BG/TrackSprite2.region_rect.position.x = dist + 128
	if dist < -256:
		dist = 0
		gen_256(0.0)

func add_to_dead_characters(node):
	node.get_parent().call_deferred("remove_child", node)
	$DeadCharacters.call_deferred("add_child", node)

func new_game():
	clear()
	total_dist = 0.0
	set_up_chars()
	gen_256(32)
	player.reset()
	player.get_parent().call_deferred("remove_child", player)
	$Characters.call_deferred("add_child", player)

func clear():
	dist = 0.0
	var hurdles = $Hurdles.get_children()
	for h in hurdles:
		h.free()
	var powerups = $Powerups.get_children()
	for p in powerups:
		p.free()
	var dead_characters = $DeadCharacters.get_children()
	for d in dead_characters:
		if !d.is_in_group("player"):
			d.free()
	var characters = $Characters.get_children()
	for c in characters:
		if !c.is_in_group("player"):
			c.free()

func set_up_chars():
	for i in range(1,5):
		var c = packed_character.instance()
		c.lane = i
		c.position.y = -50.0 + i * 25.0
		c.position.x = 0
		$Characters.add_child(c)
