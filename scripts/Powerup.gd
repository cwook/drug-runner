extends Area2D

enum EFF {UPPER, DOWNER, HALLUCINOGEN}
export(EFF) var effect
export(float) var duration
export(float) var strength

func _ready():
	set_process(true)

func _process(dt):
	self.position.x += Track.speed * dt

	if self.position.x > 280:
		self.queue_free()
		set_process(false)

func _on_Powerup_body_entered(body):
	if body.is_in_group("player"):
		self.visible = false
		set_process(false)
		$CollisionShape2D.disabled = true
		body.add_powerup(effect, strength, duration, $Sprite.texture)
		self.queue_free()
