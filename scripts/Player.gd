tool

extends "Character.gd"

var PowerupClass = load("res://scripts/Powerup.gd")
var powerups = []

var base_speed = 30
var heart_rate = 60
var hallucinogen = 0.0

func _ready():
	speed = base_speed

func _update(dt):
	if !fallen:
		if base_speed != 0:
			heart_rate += dt * (speed / base_speed)
		heart_rate = clamp(heart_rate, 60.0,200.0)
		
		Track.HUD.heart_rate = heart_rate
		
		speed = base_speed
		
		var desired_hall = 0.0
		
		var i = 0
		while i < powerups.size():
			powerups[i].time -= dt
			if powerups[i].time <= 0:
				powerups.remove(i)
				Track.HUD.update_powerups()
				continue
			match powerups[i].effect:
				PowerupClass.EFF.UPPER:
					speed += powerups[i].strength
				PowerupClass.EFF.DOWNER:
					speed -= powerups[i].strength
				PowerupClass.EFF.HALLUCINOGEN:
					desired_hall += powerups[i].strength
			i += 1
		speed = clamp(speed, 10.0, 90.0)
		
		var hall = Track.get_node("Camera2D/HallucinogenEffect").material.get_shader_param("power")
		Track.get_node("Camera2D/HallucinogenEffect").material.set_shader_param("power", lerp(hall, desired_hall, dt*3))
		
		if Input.is_action_just_pressed("jump") and !jumping:
			.jump()
		if !jumping:
			if Input.is_action_just_pressed("lane_up"):
				if position.y - (-45 + lane * 25) < 1:
					lane -= 1
			if Input.is_action_just_pressed("lane_down"):
				if position.y - (-45 + lane * 25) > -1:
					lane += 1
	
	Track.speed = speed
	Track.HUD.powerups = powerups

func fall():
	print("player fall")
	#.fall()
	base_speed = 0
	speed = 0
	powerups.clear()
	Track.HUD.update_powerups()
	Game.game_in_progress = false
	.fall()

func add_powerup(effect, strength, duration, texture):
	powerups.append({effect = effect, strength=strength, time=duration, total_time=duration, texture=texture})
	if effect == PowerupClass.EFF.HALLUCINOGEN:
		heart_rate -= strength
	heart_rate = clamp(heart_rate, 60.0,200.0)
	Track.HUD.powerups = powerups
	Track.HUD.update_powerups()

func reset():
	lane = 0
	position.y = -45 + lane * 25
	fallen = false
	base_speed = 30
	speed = base_speed
	print("reset")
	heart_rate = 60.0
	powerups = []
	self.position.x = 0
	$AnimationPlayer.play("Run")
	$CollisionShape2D.disabled = false
	Track.get_node("Camera2D/HallucinogenEffect").material.set_shader_param("power", 0.0)
