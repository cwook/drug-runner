tool

extends Line2D

func _ready():
	set_process(true)

func _process(dt):
	set_point_position(0, get_node("../pole1").position)
	set_point_position(1, get_node("../pole2").position)
