extends Area2D

func _ready():
	set_process(true)

func _process(dt):
	self.position.x += Track.speed * dt
	$pole1.position.x = 7 * (self.position.x / 128) * ((self.position.y+128) / 256)
	$pole2.position.x = -7 * (self.position.x / 128) * ((self.position.y+128) / 256)
	
	if self.position.x > 280:
		self.queue_free()
		set_process(false)


func _on_Hurdle_body_entered(body):
	if body.is_in_group("character"):
		if !body.fallen:
			body.fall()
