tool

extends "Character.gd"

var skill : int = 0
export(bool) var use_override_skill = false
export(int, 0, 10) var override_skill = 3
export(bool) var use_override_speed = false
export(int, 10, 60) var override_speed = 30
var base_react = 0.03
var react = 0.03
var react_timer = 0.0
var eye_timer = 0.0
func _ready():
	if !Engine.editor_hint:
		randomize()
		if use_override_skill:
			skill = override_skill
		else:
			skill = randi() % 11
		if use_override_speed:
			speed = override_speed
		else:
			speed = int( pow(float(randi()%60), 2.0) / 72.0 + 10.0 )
		base_react = 1.0 - pow(0.15 * float(skill), 1.6) / 2.0
		react = base_react + 0.5 * pow(randf()-1.0, 3.0)
		$RayCast2D.enabled = true

func _update(dt):
	if !fallen:
		# AI
		if $RayCast2D.is_colliding():
			if $RayCast2D.get_collider().is_in_group("hurdle"):
				react_timer += dt
			if react_timer >= react and !jumping:
				jump()
				react = base_react + 0.3 * pow(randf()-1.0, 3.0)
				react_timer = 0.0