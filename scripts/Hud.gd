extends Container

var heart_rate : float  = 60.0
var heart_timer : float = 0.0
var beat = false
var beat_timer : float = 1.0 / heart_rate

var timer_packed = preload("res://scenes/Timer.tscn")
var powerups = []

var total_dist : int = 1

onready var Game = get_parent()

func _ready():
	set_process(true)

func _process(dt):
	match Game.state:
		Game.BOOT:
			$Game.visible = false
			$Menu.visible = false
			$Result.visible = false
			$Disclaimer.visible = true
		Game.MENU:
			$Game.visible = false
			$Menu.visible = true
			$Result.visible = false
			$Disclaimer.visible = false
		Game.RESULT:
			$Game.visible = false
			$Menu.visible = false
			$Result.visible = true
			$Disclaimer.visible = false
		Game.PLAYING:
			$Menu/Resume.visible = Game.game_in_progress
			$Game.visible = true
			$Menu.visible = false
			$Result.visible = false
			$Disclaimer.visible = false
			
			$Game/Lower/Distance.text = "Distance: " + String(total_dist)
			
			var powerupUI = $Game/Upper/Powerups.get_children()
			for i in range(powerups.size()):
				powerupUI[i].get_node("timerSprite").frame = int(floor(33.0 - 33.0 * (powerups[i].time / powerups[i].total_time) ))
			
			$Game/Upper/HeartRate.bbcode_text = "[center]" + String(int(heart_rate)) + "[/center]"
			heart_timer += dt
			if heart_timer >= (60.0 / heart_rate):
				$Game/Upper/heart.frame = 1
				beat = true
				beat_timer = 0.0
				heart_timer = 0.0
			
			if beat:
				beat_timer += dt
				if beat_timer > 10.0 / heart_rate:
					$Game/Upper/heart.frame = 0
					beat = false
		Game.RESULT:
			pass

func update_powerups():
	var children = $Game/Upper/Powerups.get_children()
	for c in children:
		c.queue_free()
	for p in powerups:
		var t = timer_packed.instance()
		var s = Sprite.new()
		s.offset = Vector2(12,14)
		s.texture = p.texture
		t.add_child(s)
		t.get_node("timerSprite").frame = int(33 - floor( 33.0 * (p.time / p.total_time) ))
		$Game/Upper/Powerups.add_child(t)
		#t.add_child(

func _on_AcceptDisclaimer_pressed():
	Game.state = Game.MENU

func _on_RejectDisclaimer_pressed():
	get_tree().quit()

func _on_NewGame_pressed():
	Game.new_game()
	update_powerups()

func _on_Resume_pressed():
	Game.state = Game.PLAYING

func _on_HelpBack_pressed():
	$Menu/HelpMenu.visible = false

func _on_Help_pressed():
	$Menu/HelpMenu.visible = true
	$Menu/HelpMenu/ScrollContainer.scroll_vertical = 0

func _on_Quit_pressed():
	get_tree().quit()